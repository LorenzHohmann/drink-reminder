drinkReminderApp.controller('intervalController', function($scope, $timeout, history) {
	$scope.messageState = 'danger';
	$scope.interval = 45;

	$scope.reminderOnStart = function() {
		const interval = localStorage.getItem('interval');
		if(interval > 0) {
			$scope.interval = parseInt(interval);
		}
		$scope.toggleReminderState();

		$scope.lastDrink = history.getLastDrink();
	}

	$scope.inputKeyUp = function(value) {
		if($scope.intervalActive) {
			$scope.toggleReminderState();
		}

		if(value > 120) {
			$scope.interval = 120;

			$scope.showMessage('Es macht keinen Sinn den Interval über zwei Stunden einzustellen! Ich habe ihn für dich auf zwei Stunden gestellt! :)', 3000, 'warning');
		}

		localStorage.setItem('interval', $scope.interval);
	}

	$scope.toggleReminderState = function() {

		if($scope.intervalActive) {
			$scope.intervalActive = false;

			$scope.timerInSeconds = 0;
			$scope.stopTimer();
			if (window.cordova) {
				cordova.plugins.notification.local.cancel();
				cordova.plugins.notification.un('click', function() { });
			}

			$scope.showMessage('Der Reminder wurde gestoppt!', 5000);
		} else {
			let activationMessage = 'Der Reminder wurde gestartet!';
			$scope.intervalActive = true;

			if($scope.interval == undefined || $scope.interval < 1) {
				$scope.interval = 10;
				activationMessage += ' Ich habe den Interval auf ' + $scope.interval + ' Minuten gesetzt, da du einen ungültigen Wert eingegeben hast.';
			}

			$scope.timerInSeconds = $scope.interval * 60;
			$scope.startTimer();

			if (window.cordova) {
				cordova.plugins.notification.local.schedule({
				    title: 'Hast du nicht etwas vergessen..?',
				    text: 'Du solltest mal wieder einen Schluck trinken!',
				    trigger: { every: $scope.interval, unit: 'minute' },
				    actions: [
						{ id: 'done', title: 'Erledigt!' }
					]
				});

				cordova.plugins.notification.local.on('done', function (notification, eopts) {
					history.save();
					$scope.showMessage('Super! Du bist auf einem guten Weg deinen täglichen Flüssigkeitsbedarf zu erreichen! :)', 5000);
					$scope.lastDrink = history.getLastDrink();
				});
			}
			$scope.showMessage(activationMessage, 5000);
		}
	}

	$scope.startTimer = function() {
		$scope.timer = setInterval(() => {
			if($scope.timerInSeconds > 0) {
				$scope.timerInSeconds--;
				$scope.$digest();
			} else {
				$scope.timerInSeconds = $scope.interval * 60;
				$scope.$digest();
			}
		}, 1000);
	}

	$scope.stopTimer = function() {
		clearInterval($scope.timer);
		$scope.timer = false;
	}

	$scope.showMessage = function(message, delay = 3000, state = 'success') {

		$scope.showMsg = true;
		$scope.message = message;
		$scope.messageState = state;

		$timeout(() => {
			$scope.message = '';
			$scope.showMsg = false;
		}, delay);
	}

	$scope.getFormattedTime = function(seconds) {
		let minutes = 0;

		while(seconds > 59) {
			minutes++;
			seconds -= 60;
		}

		if(minutes < 10) {
			minutes = '0' + minutes;
		}

		if(seconds < 10) {
			seconds = '0' + seconds;
		}

		return minutes + ":" + seconds;
	}
});