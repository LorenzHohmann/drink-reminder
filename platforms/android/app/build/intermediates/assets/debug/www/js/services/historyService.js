drinkReminderApp.service('history', function() {
	
	this.save = function() {
		let data = JSON.parse(localStorage.getItem('history'));

		if(data == null) {
			data = new Array();			
		}

		const currentDate = Date.now();
		data.push(currentDate);

		localStorage.setItem('history', JSON.stringify(data));
	}

	this.getLastDrink = function() {
		let data = JSON.parse(localStorage.getItem('history'));

		if(data != null) {
			const timestamp = new Date(data[data.length-1]);
			const date = timestamp.toLocaleDateString('de-DE');
			const time = timestamp.toLocaleTimeString('de-DE');
			return date + ' ' + time;
		}

		return null;
	}
});